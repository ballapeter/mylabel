# MongoDB in Laravel 9

## Create Database

    > php artisan migrate

## Install MongoDB extension on Windows (WAMP)

### 1. Download the current 64 Thread version mongodb DLL

https://pecl.php.net/package/mongodb

### 2. Copy the .dll and .pdp files into the used php version ext folder e.g.: C:\wamp64\bin\php\php8.1.0\ext

### 3. Add to the php.ini 

    extension=mongodb
