<?php

use App\Http\Controllers\LabelController;
use Illuminate\Support\Facades\Route;

//LABEL
Route::get('/labels/queue-size', [LabelController::class, 'queryQueueSize']);
Route::post('/labels/prepare', [LabelController::class, 'prepare']);
Route::get('/labels/upload', [LabelController::class, 'upload']);
Route::get('/labels/download', [LabelController::class, 'download']);
