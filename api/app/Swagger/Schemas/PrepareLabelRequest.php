<?php

namespace App\Swagger\Schemas;

use OpenApi\Attributes as OA;

#[OA\Schema(
    description: 'Prepare Label request',
    required: [
        'recipientName',
        'recipientZipCode',
        'recipientCity',
        'recipientStreet',
        'recipientEmail',
        'recipientPhone'
    ]
)]
class PrepareLabelRequest
{
    #[OA\Property(
        title: 'senderName',
        format: 'string',
        example: 'John Sender'
    )]
    private string $senderName;

    #[OA\Property(
        title: 'senderEmail',
        format: 'string',
        example: 'test.sender@gmail.com'
    )]
    private string $senderEmail;

    #[OA\Property(
        title: 'recipientName',
        format: 'string',
        example: 'Jane Recipient'
    )]
    private string $recipientName;

    #[OA\Property(
        title: 'recipientZipCode',
        format: 'string',
        example: '4033'
    )]
    private string $recipientZipCode;

    #[OA\Property(
        title: 'recipientCity',
        format: 'string',
        example: 'Debrecen'
    )]
    private string $recipientCity;

    #[OA\Property(
        title: 'recipientStreet',
        format: 'string',
        example: 'Test street 69.'
    )]
    private string $recipientStreet;

    #[OA\Property(
        title: 'recipientEmail',
        format: 'string',
        example: 'test@gmail.com'
    )]
    private string $recipientEmail;

    #[OA\Property(
        title: 'recipientPhone',
        format: 'string'
    )]
    private string $recipientPhone;
}
