<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use MailerSend\MailerSend;
use PhpAmqpLib\Connection\AMQPSSLConnection;

class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(MailerSend::class, static function () {
            return new MailerSend(['api_key' => env('MAILERSEND_API_KEY')]);
        });

        $this->app->singleton(AMQPSSLConnection::class, static function () {
            $sslOptions = array(
                'cafile' => realpath(__DIR__ . '/../../cert/isrgrootx1.pem'),
            );

            return new AMQPSSLConnection(
                env('CLOUDAMQP_RABBITMQ_HOST'),
                env('CLOUDAMQP_RABBITMQ_PORT'),
                env('CLOUDAMQP_RABBITMQ_USER'),
                env('CLOUDAMQP_RABBITMQ_PASSWORD'),
                env('CLOUDAMQP_RABBITMQ_VHOST'),
                $sslOptions
            );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
