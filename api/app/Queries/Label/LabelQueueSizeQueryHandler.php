<?php

namespace App\Queries\Label;

use App\Queries\QueryHandlerInterface;
use PhpAmqpLib\Connection\AMQPSSLConnection;

class LabelQueueSizeQueryHandler implements QueryHandlerInterface
{
    public function __construct(
        private readonly AMQPSSLConnection $connection
    ) {
    }

    public function execute(): int
    {
        $channel = $this->connection->channel();

        //If it exists then we dont need to declare
        $queueInfo = $channel->queue_declare(
            'labels_queue',
            false,
            true, //After a RabbitMQ crash or just stop the saved messages stores. If durable false, than temporary
            false,
            false
        );

        $channel->close();
        $this->connection->close();

        return $queueInfo[1];
    }
}
