<?php

namespace App\Queries;

interface QueryHandlerInterface
{
    public function execute();
}
