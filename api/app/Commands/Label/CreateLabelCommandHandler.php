<?php

namespace App\Commands\Label;

use App\Commands\CommandHandlerInterface;
use App\Commands\CommandInterface;
use App\Services\MailerSend\MailerSendDownloadLabelMail;
use Illuminate\Support\Facades\Storage;
use Imagick;
use ImagickDraw;
use ImagickPixel;
use MailerSend\MailerSend;

class CreateLabelCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private readonly MailerSend $mailerSend
    ) {
    }

    public function execute(CommandInterface $command): bool
    {
        if (!($command instanceof CreateLabelCommand)) {
            return false;
        }

        //Upload to S3
        $templatePath = __DIR__ . '/../../../resources/views/label_template.jpg';
        $tempFile = fopen(__DIR__ . '/label.jpg', 'w');//TODO find a better place
        $fontSize = 26;
        $fontColor = new ImagickPixel('#222');

        $image = new Imagick($templatePath);

        //Sender name
        $drawSenderName = new ImagickDraw();
        $drawSenderName->setFontSize($fontSize);
        $drawSenderName->setFillColor($fontColor);
        $image->annotateImage($drawSenderName, 150, 225, 0, $command->senderName);

        //Sender email
        $drawSenderEmail = new ImagickDraw();
        $drawSenderEmail->setFontSize($fontSize);
        $drawSenderEmail->setFillColor($fontColor);
        $image->annotateImage($drawSenderEmail, 150, 265, 0, $command->senderEmail);

        //Recipient name
        $drawRecipientName = new ImagickDraw();
        $drawRecipientName->setFontSize($fontSize);
        $drawRecipientName->setFillColor($fontColor);
        $image->annotateImage($drawRecipientName, 150, 435, 0, $command->recipientName);

        //Recipient email
        $drawRecipientEmail = new ImagickDraw();
        $drawRecipientEmail->setFontSize($fontSize);
        $drawRecipientEmail->setFillColor($fontColor);
        $image->annotateImage($drawRecipientEmail, 150, 470, 0, $command->recipientEmail);

        //Recipient phone
        $drawRecipientPhone = new ImagickDraw();
        $drawRecipientPhone->setFontSize($fontSize);
        $drawRecipientPhone->setFillColor($fontColor);
        $image->annotateImage($drawRecipientPhone, 150, 510, 0, $command->recipientPhone);

        //Recipient address
        $drawRecipientAddress = new ImagickDraw();
        $drawRecipientAddress->setFontSize($fontSize);
        $drawRecipientAddress->setFillColor($fontColor);
        $image->annotateImage(
            $drawRecipientAddress,
            180,
            550,
            0,
            sprintf(
                "%s %s %s",
                $command->recipientZipCode,
                $command->recipientCity,
                $command->recipientStreet
            )
        );

        $image->writeImageFile($tempFile);

        $s3Filename = time() . '_label.jpg';

        $result = Storage::disk('s3')->put($s3Filename, file_get_contents(__DIR__ . '/label.jpg'));

        if (!$result) {
            return false;
        }

        (new MailerSendDownloadLabelMail(
            $this->mailerSend,
            $command->senderEmail,
            $command->senderName,
            'https://mylabel-api.peterballa.hu/api/labels/download?label=' . $s3Filename
        ))->send();

        return true;
    }
}
