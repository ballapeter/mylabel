<?php

namespace App\Commands\Label;

use App\Commands\CommandHandlerInterface;
use App\Commands\CommandInterface;
use PhpAmqpLib\Connection\AMQPSSLConnection;
use PhpAmqpLib\Message\AMQPMessage;

class PrepareLabelCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private readonly AMQPSSLConnection $connection
    ) {
    }

    public function execute(CommandInterface $command): bool
    {
        if (!($command instanceof PrepareLabelCommand)) {
            return false;
        }

        $channel = $this->connection->channel();

        //If it exists then we dont need to declare
        $channel->queue_declare(
            env('CLOUDAMQP_RABBITMQ_QUEUE_NAME'),
            false,
            true, //After a RabbitMQ crash or just stop the saved messages stores. If durable false, than temporary
            false,
            false
        );

        $data = [
            'senderName' => $command->senderName,
            'senderEmail' => $command->senderEmail,
            'recipientName' => $command->recipientName,
            'recipientZipCode' => $command->recipientZipCode,
            'recipientCity' => $command->recipientCity,
            'recipientStreet' => $command->recipientStreet,
            'recipientEmail' => $command->recipientEmail,
            'recipientPhone' => $command->recipientPhone
        ];

        $msg = new AMQPMessage(
            json_encode($data),
            [
                'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
            ] //Need if we want store message after RabbitMQ save
        );

        //exchange is the default "direct"
        $channel->basic_publish($msg, '', env('CLOUDAMQP_RABBITMQ_QUEUE_NAME'));

        $channel->close();
        $this->connection->close();

        return true;
    }
}
