<?php

namespace App\Commands\Label;

use App\Commands\CommandInterface;

class CreateLabelCommand implements CommandInterface
{
    public function __construct(
        public readonly string $senderName,
        public readonly string $senderEmail,
        public readonly string $recipientName,
        public readonly string $recipientZipCode,
        public readonly string $recipientCity,
        public readonly string $recipientStreet,
        public readonly string $recipientEmail,
        public readonly string $recipientPhone
    ) {
    }
}
