<?php

namespace App\Commands;

interface CommandHandlerInterface
{
    public function execute(CommandInterface $command);
}
