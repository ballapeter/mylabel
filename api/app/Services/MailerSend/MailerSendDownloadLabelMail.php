<?php

namespace App\Services\MailerSend;

use MailerSend\Helpers\Builder\EmailParams;
use MailerSend\Helpers\Builder\Recipient;
use MailerSend\Helpers\Builder\Variable;
use MailerSend\MailerSend;

class MailerSendDownloadLabelMail implements MailerSendMailInterface
{
    public function __construct(
        private readonly MailerSend $mailerSend,
        private readonly string $email,
        private readonly string $name,
        private readonly string $downloadLink
    ) {
    }

    public function send(): array
    {
        $variables = [
            new Variable($this->email, [
                'name' => $this->name,
                'downloadLink' => $this->downloadLink
            ])
        ];

        $recipients = [
            new Recipient($this->email, $this->name),
        ];

        $emailParams = (new EmailParams())
            ->setFrom('mail@mylabel-api.peterballa.hu')
            ->setFromName('Peter Balla')
            ->setRecipients($recipients)
            ->setSubject('Download package label for shipping')
            ->setTemplateId('jy7zpl957d045vx6')
            ->setVariables($variables);

        return $this->mailerSend->email->send($emailParams);
    }
}
