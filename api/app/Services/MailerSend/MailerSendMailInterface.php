<?php

namespace App\Services\MailerSend;

interface MailerSendMailInterface
{
    public function send(): array;
}
