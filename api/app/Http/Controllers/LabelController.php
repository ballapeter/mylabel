<?php

namespace App\Http\Controllers;

use App\Commands\Label\PrepareLabelCommand;
use App\Commands\Label\PrepareLabelCommandHandler;
use App\Http\Requests\Commands\Label\PrepareRequest;
use App\Queries\Label\LabelQueueSizeQueryHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;
use OpenApi\Attributes as OA;

class LabelController extends Controller
{
    #[OA\Get(
        path: '/api/labels/queue-size',
        operationId: 'queryQueueSize',
        summary: 'Query the size of labels in the queue',
        tags: ['Labels'],
        responses: [
            new OA\Response(
                response: 200,
                description: 'successful operation',
                content: [
                    new OA\JsonContent(
                        properties: [
                            new OA\Property(property: 'sizeOfQueue', type: 'integer')
                        ],
                        type: 'object'
                    )
                ]
            ),
            new OA\Response(response: 422, description: 'validation error')
        ]
    )]
    public function queryQueueSize(LabelQueueSizeQueryHandler $queryHandler): JsonResponse
    {
        return new JsonResponse(['sizeOfQueue' => $queryHandler->execute()], Response::HTTP_OK);
    }

    #[OA\Post(
        path: '/api/labels',
        operationId: 'prepare',
        summary: 'Prepare label by sending datas into queue.',
        requestBody: new OA\RequestBody(
            description: 'Prepare label by sending datas into queue.',
            required: true,
            content: new OA\JsonContent(ref: '#/components/schemas/PrepareLabelRequest')
        ),
        tags: ['Labels'],
        responses: [
            new OA\Response(
                response: 200,
                description: 'successful operation',
                content: [
                    new OA\JsonContent(
                        properties: [
                            new OA\Property(property: 'success', type: 'boolean')
                        ],
                        type: 'object'
                    )
                ]
            ),
            new OA\Response(
                response: 422,
                description: 'validation exception'
            )
        ]
    )]
    public function prepare(PrepareRequest $request, PrepareLabelCommandHandler $commandHandler): JsonResponse
    {
        $command = new PrepareLabelCommand(
            $request->getSenderName(),
            $request->getSenderEmail(),
            $request->getRecipientName(),
            $request->getRecipientZipCode(),
            $request->getRecipientCity(),
            $request->getRecipientStreet(),
            $request->getRecipientEmail(),
            $request->getRecipientPhone()
        );

        return new JsonResponse(['success' => $commandHandler->execute($command)], Response::HTTP_CREATED);
    }

    //TODO create swagger
    public function download(Request $request): Response
    {
        $file = Storage::disk('s3')->get($request->query->get('label'));

        $headers = [
            'Content-Type' => 'image/jpeg',
            'Content-Description' => 'File Transfer',
            'Content-Disposition' => "attachment; filename=label.jpg",
            'filename'=> 'label.jpg'
        ];

        return response($file, 200, $headers);
    }
}
