<?php

namespace App\Http\Requests\Commands\Label;

use Illuminate\Foundation\Http\FormRequest;

class PrepareRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'senderName' => 'required|string',
            'senderEmail' => 'required|string',
            'recipientName' => 'required|string',
            'recipientZipCode' => 'required|string',
            'recipientCity' => 'required|string',
            'recipientStreet' => 'required|string',
            'recipientEmail' => 'required|string',
            'recipientPhone' => 'required|string'
        ];
    }

    public function getSenderName(): string
    {
        return $this->get('senderName');
    }

    public function getSenderEmail(): string
    {
        return $this->get('senderEmail');
    }

    public function getRecipientName(): string
    {
        return $this->get('recipientName');
    }

    public function getRecipientZipCode(): string
    {
        return $this->get('recipientZipCode');
    }

    public function getRecipientCity(): string
    {
        return $this->get('recipientCity');
    }

    public function getRecipientStreet(): string
    {
        return $this->get('recipientStreet');
    }

    public function getRecipientEmail(): string
    {
        return $this->get('recipientEmail');
    }

    public function getRecipientPhone(): string
    {
        return $this->get('recipientPhone');
    }
}
