<?php

namespace App\Http\Middleware;

use Closure;

class XAuthorizationHeader
{
    public function handle($request, Closure $next) {

        $xAuthUppercase = $request->header('X-Authorization');

        if($xAuthUppercase !== null) {
            $request->headers->set('Authorization', 'Bearer ' . $xAuthUppercase);

            return $next($request);
        }

        $xAuthLowercase = $request->header('x-authorization');

        if($xAuthLowercase !== null) {
            $request->headers->set('Authorization', 'Bearer ' . $xAuthLowercase);
        }

        return $next($request);
    }
}
