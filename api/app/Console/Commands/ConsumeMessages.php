<?php

namespace App\Console\Commands;

use App\Commands\Label\CreateLabelCommand;
use App\Commands\Label\CreateLabelCommandHandler;
use App\Queries\Label\LabelQueueSizeQueryHandler;
use Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPSSLConnection;
use PhpAmqpLib\Message\AMQPMessage;

class ConsumeMessages extends Command
{
    protected $signature = 'consume:messages';
    protected $description = 'This command consume message from queue and create label to S3 than send it in email';

    public function __construct(
        private readonly AMQPSSLConnection $connection,
        private readonly CreateLabelCommandHandler $commandHandler,
        private readonly LabelQueueSizeQueryHandler $queryHandler
    ) {
        parent::__construct();
    }

    public function handle(): void
    {
        $currentSizeOfQueue = $this->queryHandler->execute();
        $messageLimit = min($currentSizeOfQueue, 20);
        $channel = $this->connection->channel();

        $channel->basic_qos(null, $messageLimit, null);

        $callback = function (AMQPMessage $message) {
            echo ' [x] Received ', $message->body, "\n";

            $data = json_decode($message->body, true);

            if (
                !isset(
                    $data['senderName'],
                    $data['senderEmail'],
                    $data['recipientName'],
                    $data['recipientZipCode'],
                    $data['recipientCity'],
                    $data['recipientStreet'],
                    $data['recipientEmail'],
                    $data['recipientPhone']
                )
            ) {
                //TODO logging and/or error handling
                $message->ack();
                return;
            }

            $command = new CreateLabelCommand(
                $data['senderName'],
                $data['senderEmail'],
                $data['recipientName'],
                $data['recipientZipCode'],
                $data['recipientCity'],
                $data['recipientStreet'],
                $data['recipientEmail'],
                $data['recipientPhone']
            );

            $result = $this->commandHandler->execute($command);

            if (!$result) {
                echo $data['senderName'] . "'s message not processed\n";
            }

            $message->ack(); //Necessary to acknowledge the success procession to queue
        };

        $channel->basic_consume(
            env('CLOUDAMQP_RABBITMQ_QUEUE_NAME'),
            '',
            false,
            false, //Need to be false to ensure have no lost message no_ack = no acknowlegement
            false,
            false,
            $callback
        );

        $consumed = 0;

        while ($consumed < $messageLimit) {
            $channel->wait();
            $consumed++;
        }

        $channel->close();
        $this->connection->close();
    }
}
