import RequestPayload from '@/util/RequestPayload';
// @ts-ignore
import ResponseInterface from '~/util/ResponseInterface';

class Request {
  public async get (
    url: string,
    token?: string | null
  ): Promise<ResponseInterface> {
    const headers: { [key: string]: string } = {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    };

    if (token !== null) {
      headers.Authorization = `Bearer ${token}`;
    }

    const response: Response = await fetch(url, {
      method: 'GET',
      headers
    });

    return {
      response,
      data: await response.json()
    }
  }

  public async post (
    url: string,
    payload: RequestPayload,
    token: string | null = null
  ): Promise<ResponseInterface> {
    const headers: { [key: string]: string } = {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    };

    if (token !== null) {
      headers.Authorization = `Bearer ${token}`;
    }

    const response: Response = await fetch(url, {
      method: 'POST',
      headers,
      body: JSON.stringify(payload)
    });

    return {
      response,
      data: await response.json()
    }
  }

  public async put (
    url: string,
    payload: RequestPayload,
    token?: string | null
  ): Promise<ResponseInterface> {
    const headers: { [key: string]: string } = {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    };

    if (token !== null) {
      headers.Authorization = `Bearer ${token}`;
    }

    const response = await fetch(url, {
      method: 'PUT',
      headers,
      body: JSON.stringify(payload)
    });

    return {
      response,
      data: await response.json()
    }
  }

  public async patch (
    url: string,
    payload: RequestPayload,
    token?: string | null
  ): Promise<ResponseInterface> {
    const headers: { [key: string]: string } = {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    };

    if (token !== null) {
      headers.Authorization = `Bearer ${token}`;
    }

    const response = await fetch(url, {
      method: 'PATCH',
      headers,
      body: JSON.stringify(payload)
    });

    return {
      response,
      data: await response.json()
    }
  }

  public async delete (
    url: string,
    token?: string | null
  ): Promise<ResponseInterface> {
    const headers: { [key: string]: string } = {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    };

    if (token !== null) {
      headers.Authorization = `Bearer ${token}`;
    }

    const response: Response = await fetch(url, {
      method: 'DELETE',
      headers
    });

    return {
      response,
      data: await response.json()
    }
  }
}

export default new Request();
