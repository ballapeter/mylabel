import RequestPayload from '~/util/RequestPayload';

export default class PrepareLabelRequest implements RequestPayload {
  public constructor (
        public senderName: string = 'Test Sender Name',
        public senderEmail: string = '',
        public recipientName: string = 'Test Recipient Name',
        public recipientZipCode: string = '4033',
        public recipientCity: string = 'Debrecen',
        public recipientStreet: string = 'Test street 5',
        public recipientEmail: string = 'test@gmail.com',
        public recipientPhone: string = '0630xxxxx'
  ) {
  }
}
