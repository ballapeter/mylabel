import { defineStore } from 'pinia';
import { useRuntimeConfig } from '#app';
import PrepareLabelRequest from '~/stores/label/PrepareLabelRequest';
import ResponseInterface from '~/util/ResponseInterface';
import Request from '~/util/Request';

const runtimeConfig = useRuntimeConfig();

export const useLabelStore = defineStore('labelStore', {
  actions: {
    async queryQueueSize (): Promise<ResponseInterface> {
      return await Request.get(
                `${runtimeConfig.public.apiUrl}/labels/queue-size`
      )
    },
    async prepare (request: PrepareLabelRequest): Promise<ResponseInterface> {
      return await Request.post(
                `${runtimeConfig.public.apiUrl}/labels/prepare`,
                request
      );
    }
  }
})
