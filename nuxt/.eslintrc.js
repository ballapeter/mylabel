module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  extends: [
    '@nuxtjs/eslint-config-typescript'
  ],
  plugins: [
  ],
  // add your custom rules here
  rules: {
    semi: 'off',
    'no-useless-constructor': 'off',
    'vue/no-multiple-template-root': 'off',
    'no-console': 'off',
    'vue/multi-word-component-names': 'off'
  }
}
