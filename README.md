# MyLabel by Peter Balla

This is simple package label generator "have fun" project of a fake delivery service to demonstrate the usage of the commonly used trendy technologies like:
- Laravel 9
- Nuxt 3 (with Vue3 and Pinia)
- TypeScript
- RabbitMQ + CloudAMQP to put necessary data for label generating in a queue
- AWS S3 for generated label storage
- MailerSend API
- Tailwind 3.2.4
- SCSS